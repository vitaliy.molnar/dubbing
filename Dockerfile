FROM mcr.microsoft.com/dotnet/core/aspnet:2.2 AS base
WORKDIR /app
ENV ASPNETCORE_ENVIRONMENT=Development
ENV ASPNETCORE_URLS http://*:5000
EXPOSE 5000

FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS builder
ARG Configuration=debug

WORKDIR /dubbing

COPY ./*.sln ./ 
COPY ./Administration/Core/*.csproj ./Administration/Core/
COPY ./Administration/Infrastructure/*.csproj ./Administration/Infrastructure/
COPY ./Administration/UnitTests/*.csproj ./Administration/UnitTests/
COPY ./Streaming/Hubs/*.csproj ./Streaming/Hubs/
COPY ./Streaming/UnitTests/*.csproj ./Streaming/UnitTests/
COPY ./Web/*.csproj ./Web/
COPY ./Web/AudioFiles ./AudioFiles
RUN dotnet restore


COPY ./ ./

WORKDIR /dubbing
RUN dotnet build -c $Configuration -o /app

FROM builder AS publish
ARG Configuration=debug
RUN dotnet publish -c $Configuration -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
RUN mkdir AudioFiles
ENTRYPOINT ["dotnet","Web.dll"]
